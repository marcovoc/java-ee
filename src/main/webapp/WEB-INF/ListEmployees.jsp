<%-- 
     Document   : EmployeeList
     Created on : 7 nov. 2019, 13:11:33
     Author     : laptop-masca
     --%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employee list</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/ListEmployees.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        
        <div id="navbar">
            <ul>
                <li><div id="hello"> </div>Hello ${appUser.username}</li>
                <li style="float: right">
                    <form method="post" action="index">
                        <input type="image" style="width: 60px; height: 60px;" src="${pageContext.request.contextPath}/image/power-button-off.png" name="SignOut" value="SignOut" alt="Sign out">
                    </form>
                </li>
            </ul>
        </div>
        <div id="titleBounding">
            <h1>Employees list</h1>
        </div>
        <div id ="tableBounding">
            <c:if test="${employees.size() != 0}" var="variable">
                <form method="post" action="index">
                    <table>
                        <thead>
                            <tr>
                                <th class="selectHeader">Select</th>
                                <th>Id</th>
                                <th>Last name</th>
                                <th>First name</th>
                                <th>Home number</th>
                                <th>Personnal number</th>
                                <th>Professional number</th>
                                <th>City</th>
                                <th>Adress</th>
                                <th>ZIP code</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${ employees }" var="employe" varStatus="status">
                                <tr>
                                    <td><input type="radio" value='${ employe.id }' name="selectRadio"></td>
                                    <td><c:out value="${ employe.id }" /></td>
                                    <td><c:out value="${ employe.nom }" /></td>
                                    <td><c:out value="${ employe.prenom }" /></td>
                                    <td><c:out value="${ employe.teldom }" /></td>
                                    <td><c:out value="${ employe.telport }" /></td>
                                    <td><c:out value="${ employe.telpro }" /></td>
                                    <td><c:out value="${ employe.ville }" /></td>
                                    <td><c:out value="${ employe.adresse }" /></td>
                                    <td><c:out value="${ employe.codepostal }" /></td>
                                    <td><c:out value="${ employe.email }" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${appUser.isadmin}" var="variable">
                        <input type="submit" class="btn btn-danger btnPerso" name="action" value="Delete">
                    </c:if>
                    <input type="submit" class="btn btn-light btnPerso" name="action" value="Details">
                    <c:if test="${appUser.isadmin}" var="variable">
                        <input type="submit" class="btn btn-primary btnPerso link" name="action" value="Create">
                    </c:if>
                    <c:if test="${error != null}" var="variable">
                        <span class="error">
                            ${error}
                        </span>
                    </c:if>
                    <c:if test="${succes != null}" var="variable">
                        <span class="succes">
                            ${succes}
                        </span>
                    </c:if>
                </form>
            </c:if>
            <c:if test="${employees.size() == 0}" var="variable">
                <form method="post" action="index">
                    <h1>We should hire someone</h1>
                    <c:if test="${appUser.isadmin}" var="variable">
                        <a style="margin: auto" class="btn btn-primary btnPerso link" href="${pageContext.request.contextPath}/CreateEmployee">Create</a>
                    </c:if>
                </form>
            </c:if>
        </div>
    </body>
</html>
