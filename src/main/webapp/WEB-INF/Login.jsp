<%-- 
    Document   : Login
    Created on : 24 oct. 2019, 09:52:37
    Author     : laptop-masca
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/Login.css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
              <!-- Tabs Titles -->

              
              <div class="fadeIn first">
                <h2>Login</h2>
                <!-- Icon <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" />>-->
              </div>
              <h3 style="color: red">${error}</h3>

              <!-- Login Form -->
              <form method="post" action="index">
                <input type="text" id="login" class="fadeIn second" name="login" placeholder="login" value="${param.login}">
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
                <input type="submit" class="fadeIn fourth" value="Log In">
              </form>

              <!-- Remind Passowrd 
              <div id="formFooter">
                <a class="underlineHover" href="#">Forgot Password?</a>
              </div>-->

            </div>
          </div>
    </body>
</html>
