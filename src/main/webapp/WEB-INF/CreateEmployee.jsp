<%-- 
     Document   : CreateEmploye
     Created on : 13 nov. 2019, 20:25:01
     Author     : masca
     --%>

    <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Create employe</title>
                <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/CreateEmployee.css">
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            </head>
            <body>
                <div id="navbar">
                    <ul>
                        <li><div id="hello"> </div>Hello ${appUser.username}</li>
                        <li style="float: right">
                            <form method="post" action="index">
                                <input type="image" style="width: 60px; height: 60px;" src="${pageContext.request.contextPath}/image/power-button-off.png" name="SignOut" value="SignOut" alt="Sign out">
                            </form>
                        </li>
                    </ul>
                </div>
                <div id ="tableBounding">
                    <form method="post" action="index">
                        <div class="center">
                            <label for="LastName">Last name: </label>
                            <input type="text" name="LastName" id="LastName" value="${LastName}">
                        </div>
                        <div class="center">
                            <label for="FirstName">First name: </label>
                            <input type="text" name="FirstName" id="FirstName" value="${FirstName}">
                        </div>
                        <div class="center">
                            <label for="HomeNumber">Home number: </label>
                            <input type="text" name="HomeNumber" id="HomeNumber" value="${HomeNumber}">
                        </div>
                        <div class="center">
                            <label for="PersonalNumber">Personal number : </label>
                            <input type="text" name="PersonalNumber" id="PersonalNumber" value="${PersonalNumber}">
                        </div>
                        <div class="center">
                            <label for="ProfessionalNumber">Professional number:</label>
                            <input type="text" name="ProfessionalNumber" id="ProfessionalNumber" value="${ProfessionalNumber}">
                        </div>
                        <div class="center">
                            <label for="City">City:</label>
                            <input type="text" name="City" id="City" value="${City}">
                        </div>
                        <div class="center">
                            <label for="Adress">Adress:</label>
                            <input type="text" name="Adress" id="Adress" value="${Adress}">
                        </div>
                        <div class="center">
                            <label for="ZIPCode">ZIP code:</label>
                            <input type="text" name="ZIPCode" id="ZIPCode" value="${ZIPCode}">
                        </div>
                        <div class="center">
                            <label for="Email">Email: </label>
                            <input type="Email" name="Email" id="Email" value="${Email}">
                        </div>
                        <div>
                            <input type="submit" class="btn btn-success btnPerso" name="action" value="Create">
                            <input type="submit" class="btn btn-primary btnPerso" name="action" value="See list">
                            <c:if test="${error != null}" var="variable">
                                <span class="error">
                                    ${error}
                                </span>
                            </c:if>
                        </div>
                    </form>
                </div>
            </body>
        </html>
