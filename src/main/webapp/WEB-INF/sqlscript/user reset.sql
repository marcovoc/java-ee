/**** CIBLE : Java DB ****/

/*On supprime les tables si elles existent */
DROP TABLE APPUSER;

/*Cr�ation de la table EMPLOYES*/
CREATE TABLE "APPUSER" (
	"USERNAME" VARCHAR(25) NOT NULL PRIMARY KEY,
	"PASSWORD" VARCHAR(25) NOT NULL,
        "ISADMIN" BOOLEAN NOT NULL
);

/*Insertion de 4 membres*/
INSERT INTO APPUSER(USERNAME, PASSWORD, ISADMIN) VALUES
('admin', 'admin', true),
('empl', 'empl', false);