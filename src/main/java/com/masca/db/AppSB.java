/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.db;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author laptop-masca
 */
@Stateless
public class AppSB {
    
    @PersistenceContext
    EntityManager em;
    
    public AppUser findUser(String login, String password)
    {
        Query q = em.createQuery("SELECT u FROM AppUser u WHERE :l = u.username AND :p = u.password");
        q.setParameter("l", login);
        q.setParameter("p", password);
        return (AppUser)q.getSingleResult();
    }
    
    public Employes findEmployee(int id)
    {
        Query q = em.createQuery("SELECT e FROM Employes e WHERE e.id=:i");
        q.setParameter("i", id);
        return (Employes)q.getSingleResult();
    }
    
    public List getEmployees()
    {
        Query q = em.createQuery("SELECT e FROM Employes e");
        return q.getResultList();
    }
    
    public boolean removeEmploye(int id)
    {
        try
        {
            Employes emp = em.find(Employes.class, id);
            return removeEmploye(emp);
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    public boolean removeEmploye(Employes emp)
    {
        try
        {
            em.remove(emp);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    public boolean addEmploye(Employes emp)
    {
        try
        {
            em.persist(emp);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
