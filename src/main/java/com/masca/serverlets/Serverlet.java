/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.serverlets;

import com.masca.db.AppSB;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author laptop-masca
 */
@WebServlet(name = "Index", urlPatterns = {"/index"})
public class Serverlet extends HttpServlet {
    @EJB
    AppSB appSB;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost)
            throws ServletException, IOException {
        Function func = null;
        HttpSession session = request.getSession();
        try
        {
            func = (Function)session.getAttribute(ConstValue.SESSION_FUNCTION);
        }
        finally
        {
            if(func == null)
            {
                func = Function.Login;
            }
        }
        
        switch(func)
        {
            case Login:
                Login.processRequest(request, response, isPost, this, appSB);
                break;
            case List:
                ListEmployees.processRequest(request, response, this, appSB);
                break;
            case Edit:
                EditEmployee.processRequest(request, response, isPost, this, appSB);
                break;
            case Create:
                CreateEmployee.processRequest(request, response, isPost, this, appSB);
                break;
            case SignOut:
                SignOut.processRequest(request, response);
                break;
            default:
                throw new AssertionError(func.name());
            
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, false);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, true);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
