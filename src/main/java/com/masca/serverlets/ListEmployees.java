/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.serverlets;

import com.masca.db.AppSB;
import com.masca.db.AppUser;
import com.masca.db.Employes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author laptop-masca
 */

public class ListEmployees {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param serverlet servelet wich receive the request
     * @param appSB EJB
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static void processRequest(HttpServletRequest request, HttpServletResponse response, HttpServlet serverlet, AppSB appSB)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AppUser user = (AppUser)session.getAttribute(ConstValue.SESSION_USERBEAN);
        
        if(user == null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.Login);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        if(request.getParameter("SignOut.x") != null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.SignOut);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        String action = request.getParameter("action");
        int selected =  tryParse(request.getParameter("selectRadio"), -1);
        
        if(action != null)
        {
            switch (action) {
                case "Delete":
                    if(user.getIsadmin())
                    {
                        if(selected == -1)
                        {
                            request.setAttribute(ConstValue.ATT_ERROR, "Please select an employe");
                        }
                        else
                        {
                            if(!appSB.removeEmploye(selected))
                            {
                                request.setAttribute(ConstValue.ATT_ERROR, "Employe already deleted");
                            }
                            else
                            {
                                request.setAttribute("succes", "Employe sucessfully removed");
                            }
                        }
                    }
                    break;
                case "Details":
                    if(selected == -1)
                    {
                        request.setAttribute(ConstValue.ATT_ERROR, "Please select an employe");
                    }
                    else
                    {
                        session.setAttribute(ConstValue.SESSION_FUNCTION, Function.Edit);
                        session.setAttribute(EditEmployee.SESSION_USER_SELECTED, selected);
                        response.sendRedirect(request.getContextPath() + "/index");
                        return;
                    }
                    break;
                case "Create":
                        session.setAttribute(ConstValue.SESSION_FUNCTION, Function.Create);
                        response.sendRedirect(request.getContextPath() + "/index");
                        return;
                default:
                    break;
            }
        }
        
        ArrayList<Employes> employees = new ArrayList();
        employees.addAll(appSB.getEmployees());
        request.setAttribute("employees", employees);
        
        serverlet.getServletContext().getRequestDispatcher( "/WEB-INF/ListEmployees.jsp" ).forward( request, response );
    }
    
    private static int tryParse(String value, int defaultVal) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }
}
