/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.serverlets;

import com.masca.db.AppUser;
import com.masca.db.AppSB;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author laptop-masca
 */
public class Login {

    @EJB
    private AppSB appSB;
    public static final String VIEW = "/WEB-INF/Login.jsp";
    public static final String FIELD_USERNAME = "login";
    public static final String FIELD_PASSWORD = "password";
    
    // <editor-fold desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost, HttpServlet serverlet, AppSB appSB)
            throws ServletException, IOException {
        if(!isPost)
        {
            serverlet.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );
        }
        else
        {
            String username = request.getParameter( FIELD_USERNAME );
            String password = request.getParameter( FIELD_PASSWORD );
                       
            
            if(username!= null && password != null)
            {
                if(username.isEmpty() || password.isEmpty())
                {
                    request.setAttribute(ConstValue.ATT_ERROR, "Fill all the fields");
                }
                else
                {
                    AppUser validUser = appSB.findUser(username, password);
                    HttpSession session = request.getSession();
                    if(validUser != null)
                    {
                        session.setAttribute(ConstValue.SESSION_USERBEAN, validUser);
                        session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
                        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/index");
                        requestDispatcher.forward(request, response);
                        return;
                    }
                    else
                    {
                        session.setAttribute(ConstValue.SESSION_USERBEAN, null);
                        request.setAttribute(ConstValue.ATT_ERROR, "Wrong login informations");
                    }              
                }
            }
            else
            {
                request.setAttribute(ConstValue.ATT_ERROR, "Fill all the fields");
            }
            serverlet.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );
        }
    }
}
