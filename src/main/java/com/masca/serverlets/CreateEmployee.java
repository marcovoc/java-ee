/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.serverlets;

import com.masca.db.AppSB;
import com.masca.db.AppUser;
import com.masca.db.Employes;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author masca
 */

public class CreateEmployee {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param isPost is called from a post request
     * @param serverlet serverlet wich receive the request
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost, HttpServlet serverlet, AppSB appSB)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AppUser user = (AppUser)session.getAttribute(ConstValue.SESSION_USERBEAN);

        if(user == null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.Login);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        else if (!user.getIsadmin())
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        if(request.getParameter("SignOut.x") != null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.SignOut);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        String firstName = request.getParameter("FirstName");
        String lastName = request.getParameter("LastName");
        String homeNumber = request.getParameter("HomeNumber");
        String personalNumber = request.getParameter("PersonalNumber");
        String professionalNumber = request.getParameter("ProfessionalNumber");
        String city = request.getParameter("City");
        String adress = request.getParameter("Adress");
        String ZIPCode = request.getParameter("ZIPCode");
        String email = request.getParameter("Email");
        
        request.setAttribute("FirstName", firstName);
        request.setAttribute("LastName", lastName);
        request.setAttribute("HomeNumber", homeNumber);
        request.setAttribute("PersonalNumber", personalNumber);
        request.setAttribute("ProfessionalNumber", professionalNumber);
        request.setAttribute("City", city);
        request.setAttribute("Adress", adress);
        request.setAttribute("ZIPCode", ZIPCode);
        request.setAttribute("Email", email);
        
        if(isPost)
        {
            if(request.getParameter("action") == "See list")
            {
                session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
                response.sendRedirect(request.getContextPath() + "/index");
                return;
            }
            if(firstName != null && lastName != null && homeNumber != null 
                    && personalNumber != null && professionalNumber != null 
                    && city != null && adress != null && ZIPCode != null 
                    && email != null)
            {
                Employes toAdd = new Employes(-1, firstName, lastName, homeNumber, personalNumber, professionalNumber, adress, ZIPCode, city, email);
                if(appSB.addEmploye(toAdd))
                {
                    session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
                    response.sendRedirect(request.getContextPath() + "/index");
                    return;
                }
                else
                {
                    request.setAttribute("error", "Error when try to add the employee");
                }
            }
            else
            {
                request.setAttribute("error", "Fill all fields");
            }
        }
        
        serverlet.getServletContext().getRequestDispatcher( "/WEB-INF/CreateEmployee.jsp" ).forward( request, response );
    }

}
