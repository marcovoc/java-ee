/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.masca.serverlets;

import com.masca.db.AppSB;
import com.masca.db.AppUser;
import com.masca.db.Employes;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author laptop-masca
 */
public class EditEmployee {
    
    public static final String SESSION_USER_SELECTED = "userEditId";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @param isPost process a post request
     * @param serverlet serverlet which receive the request
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPost, HttpServlet serverlet, AppSB appSB)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AppUser user = (AppUser)session.getAttribute(ConstValue.SESSION_USERBEAN);        

        if(user == null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.Login);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        Employes selectedEmploye = null;
        try
        {
            int idEmployee = (int)session.getAttribute(SESSION_USER_SELECTED);
            selectedEmploye = appSB.findEmployee(idEmployee);
            if(selectedEmploye == null)
                throw new IllegalStateException();
        }
        catch(Exception e)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
            request.setAttribute(ConstValue.ATT_ERROR, "Impossible to get the employee data for modification");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/index");
            requestDispatcher.forward(request, response);
            return;
        }
        
        if(request.getParameter("SignOut.x") != null)
        {
            session.setAttribute(ConstValue.SESSION_FUNCTION, Function.SignOut);
            response.sendRedirect(request.getContextPath() + "/index");
            return;
        }
        
        if(!isPost)
        {
            request.setAttribute("FirstName", selectedEmploye.getPrenom());
            request.setAttribute("LastName", selectedEmploye.getNom());
            request.setAttribute("HomeNumber", selectedEmploye.getTeldom());
            request.setAttribute("PersonalNumber", selectedEmploye.getTelport());
            request.setAttribute("ProfessionalNumber", selectedEmploye.getTelpro());
            request.setAttribute("City", selectedEmploye.getVille());
            request.setAttribute("Adress", selectedEmploye.getAdresse());
            request.setAttribute("ZIPCode", selectedEmploye.getCodepostal());
            request.setAttribute("Email", selectedEmploye.getEmail());
        }
        else if(isPost && !user.getIsadmin())
        {
            request.setAttribute(ConstValue.ATT_ERROR, "Only administrator can modify employees");
        }
        else if(isPost && user.getIsadmin())
        {
            String firstName = request.getParameter("FirstName");
            String lastName = request.getParameter("LastName");
            String homeNumber = request.getParameter("HomeNumber");
            String personalNumber = request.getParameter("PersonalNumber");
            String professionalNumber = request.getParameter("ProfessionalNumber");
            String city = request.getParameter("City");
            String adress = request.getParameter("Adress");
            String ZIPCode = request.getParameter("ZIPCode");
            String email = request.getParameter("Email");
            
            request.setAttribute("FirstName", firstName);
            request.setAttribute("LastName", lastName);
            request.setAttribute("HomeNumber", homeNumber);
            request.setAttribute("PersonalNumber", personalNumber);
            request.setAttribute("ProfessionalNumber", professionalNumber);
            request.setAttribute("City", city);
            request.setAttribute("Adress", adress);
            request.setAttribute("ZIPCode", ZIPCode);
            request.setAttribute("Email", email);
            
            if(firstName != null && lastName != null && homeNumber != null 
                    && personalNumber != null && professionalNumber != null 
                    && city != null && adress != null && ZIPCode != null 
                    && email != null)
            {
                Employes toEdit = new Employes(-1, firstName, lastName, homeNumber, personalNumber, professionalNumber, adress, ZIPCode, city, email);
                if(appSB.addEmploye(toEdit))
                {
                    session.setAttribute(ConstValue.SESSION_FUNCTION, Function.List);
                    response.sendRedirect(request.getContextPath() + "/index");
                    return;
                }
                else
                {
                    request.setAttribute(ConstValue.ATT_ERROR, "Error when try to edit the employee");
                }
            }
            else
            {
                request.setAttribute(ConstValue.ATT_ERROR, "Fill all fields");
            }
        }
        
        serverlet.getServletContext().getRequestDispatcher( "/WEB-INF/EditEmployee.jsp" ).forward( request, response );
    }
}
